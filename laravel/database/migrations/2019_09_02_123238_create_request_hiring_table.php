<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestHiringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_hiring', function (Blueprint $table) {
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';

          $table->bigIncrements('id');
          $table->dateTime('created_at')->nullable();
          $table->string('name')->nullable();
          $table->string('place')->nullable();
          $table->string('phonenumber', 20)->nullable();
          $table->string('email', 50);

          $table->string('language')->nullable();
          $table->string('what_does')->nullable();
          $table->string('experience')->nullable();
          $table->string('knowledge')->nullable();

          $table->text('massage')->nullable();
          $table->string('filetask')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_hiring');
    }
}
