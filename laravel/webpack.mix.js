const mix = require('laravel-mix');
mix.setPublicPath('../assets');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
  './assets/js/jquery-3.2.1.min.js',
  './assets/js/plugins.js',
  './assets/js/Popper.js',
  './assets/js/bootstrap.min.js',
  './assets/js/jquery.magnific-popup.min.js',
  './assets/js/scrollax.js',
  './assets/js/jquery.ajaxchimp.min.js',
  './assets/js/jquery.waypoints.min.js',
  './assets/js/isotope.pkgd.min.js',
  './assets/js/swiper.min.js',
  './assets/js/jquery.easypiechart.min.js',
  './assets/js/delighters.js',
  './assets/js/typed.js',
  './assets/js/jquery.parallax.js',
  './assets/js/jquery.themepunch.tools.min.js',
  './assets/js/jquery.themepunch.revolution.min.js',
  './assets/js/extensions/revolution.extension.actions.min.js',
  './assets/js/extensions/revolution.extension.kenburn.min.js',
  './assets/js/extensions/revolution.extension.layeranimation.min.js',
  './assets/js/extensions/revolution.extension.migration.min.js',
  './assets/js/extensions/revolution.extension.parallax.min.js',
  './assets/js/extensions/revolution.extension.slideanims.min.js',
  './assets/js/sweetalert2.all.min.js',
  './assets/js/bs-custom-file-input.min.js',
  './assets/js/skrollr.min.js',
  './assets/js/shuffle-letters.js',
  './assets/js/main.js',
], '../assets/js/all.js')
    .sass('./assets/sass/style.scss', './css/style.css')
  .styles([
    '../assets/css/font-awesome.min.css',
    '../assets/css/bootstrap.min.css',
    '../assets/css/animate.css',
    '../assets/css/iconfont.css',
    '../assets/css/magnific-popup.css',
    '../assets/css/owl.carousel.min.css',
    '../assets/css/owl.theme.default.min.css',
    '../assets/css/swiper.min.css',
    '../assets/css/rev-settings.css',
    '../assets/css/sweetalert2.min.css',
    '../assets/css/plugins.css',
    '../assets/css/style.css',
    '../assets/css/custom.css',
    '../assets/css/responsive.css',
  ], '../assets/css/main.css');
