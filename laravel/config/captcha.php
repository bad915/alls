<?php

return [
    'default'   => [
        'length'    => 4,
        'width'     => 120,
        'height'    => 54,
        'quality'   => 90,
        'math'      => false, //Enable Math Captcha
    ],
    // ...
];
