@extends('main')

@section('title', 'Услуги по созданию, поддержке и продвижению сайтов | Создание сайтов в Ташкенте')

@section('description', 'Услуги веб-студии в Ташкенте: сайт-визитка, интернет-магазин, landing page, информационный портал, сайт-каталог. ☎ +998 (93) 516-82-28')

@section('body')

    <header class="xs-header header-transparent nav-lights">
        <div class="container">
            <nav class="xs-menus clearfix xs_nav-landscape">
                <div class="nav-header">
                    <a class="nav-brand" href="/">
                        <img src="assets/images/logo.png" alt="" draggable="false">
                    </a>
                    <div class="nav-toggle"></div>
                </div>
                <div class="nav-menus-wrapper align-to-right"><span class="nav-menus-wrapper-close-button">✕</span>
                    <!-- menu list -->
                    <ul class="nav-menu">
                        <li class="">
                            <a href="tel:+998935168228"> <i class="fa fa-phone" aria-hidden="true"></i> +998 (93) 516-82-28</a>
                        </li>

                        <li>
                            <a class="pulsate" style="font-weight: bold; color: red;" href="/hiring">Мы нанимаем</a>
                        </li>

                        <li class="">
                            <a href="/#about">О СТУДИИ</a>
                        </li>
                        <li>
                            <a href="/services">УСЛУГИ</a>
                        </li>
                        <li>
                            <a href="/#contacts">КОНТАКТЫ</a>
                        </li>
                    </ul>
                    <!-- End menu list -->
                </div>
                <div class="nav-overlay-panel"></div></nav>
        </div><!-- .container END -->
    </header>


    <div class="xs-inner-banner inner-banner2">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mx-auto">
                    <div class="inner-banner" style="margin-top: 61.5px;">
                        <h2 class="inner-banner-title">
                            УСЛУГИ
                        </h2>

                        <ul class="breadcumbs list-inline">
                            <li><a href="/">Главная</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="xs-section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto" style="color: #000">
                    <div class="from-wraper" style="margin-top: 20px;">
                        <h3 class="text-center" style="margin-bottom: 20px;">
                            Услуги
                        </h3>

                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Создание сайта</h4>
                                <div style="padding-left: 20px;">
                                    <ul>
                                        <li>- Корпоративный сайт</li>
                                        <li>- Создание блога</li>
                                        <li>- Информационный портал</li>
                                        <li>- Интернет-магазин</li>
                                        <li>- Редизайн сайта</li>
                                        <li>- Сайт-каталог</li>
                                        <li>- Сайт-визитка</li>
                                        <li>- Landing Page</li>
                                        <li>- Мобильная версия</li>
                                        <li>- Адаптивный дизайн</li>
                                        <li>- Создание сайта «под ключ»</li>
                                        <li>- Веб-дизайн</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <h4>Обслуживание и поддержка</h4>
                                    <div style="padding-left: 20px;">
                                        <ul>
                                            <li>- Абонентское обслуживание сайта</li>
                                            <li>- Починить сайт</li>
                                            <li>- Настройка корпоративной почты, хостинг, домен</li>
                                            <li>- Веб-программирование</li>
                                            <li>- Копирайтинг</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="padding-top: 30px;">
                            <h4>Сайт «под ключ»</h4>

                            <p>В услугу сайт «под ключ» входят:</p>

                            <ol style="padding-left: 20px;">
                                <li>- Разработка концепции. </li>
                                <li>- Подбор дизайна. </li>
                                <li>- Верстка. </li>
                                <li>- Наполнение контентом. </li>
                                <li>- Проверка стабильности работы. </li>
                                <li>- Размещение на хостинге. </li>
                            </ol>

                            <p>
                                Корпоративный сайт — это существенная помощь вашему бизнесу. Это мощный инструмент продаж и возможность для мгновенного распространения информации.
                            </p>

                            <p>
                                При этом реальную пользу приносят далеко не все сайты. Некоторые из них становятся вечными аутсайдерами и не оправдывают даже затраты на разработку. Для того чтобы веб-ресурс формировал правильный имидж компании и приносил реальный доход, учитывается масса нюансов: задачи сайта и бизнеса, особенности и состав целевой аудитории, настоящее положение фирмы на рынке.
                            </p>
                        </div>

                    </div>
                </div>
            </div><!-- .row END -->
        </div>
    </div>

    <!-- footer section start -->
    <footer id="contacts" class="xs-footer-section footer-style5" data-delighter="start:0.80">
        <div class="footer-top-area">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-lg-3">
                        <div class="footer-widget">
                            <h4 class="widget-title">
                                Конакты
                            </h4>
                            <p>
                                Tel.:<a href="tel:+998935168228"> +998 (93) 516-82-28</a> <br>
                                Mail : <a href="mailto:info@alls.uz">info@alls.uz</a>
                            </p>
                            <!--
                            <ul class="social-list">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            -->
                        </div>
                    </div>

                </div><!-- .row END -->
                <div class="row">
                    <div class="col-12 text-center" style="padding-top: 60px;">
                        Copyright <?=date('Y')?> ALLS.
                    </div>
                </div>
            </div><!-- .container END -->
        </div><!-- .footer-top-area END -->

    </footer>
    <!-- footer section end -->
@endsection