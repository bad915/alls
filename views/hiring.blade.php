@extends('main')

@section('title', 'Вакансия для начинаюхих Веб-программистов с обучением и трудоустройством | Создание сайтов в Ташкенте')

@section('description', 'Работать веб-программистом в Ташкенте. Вакансия для начинаюхих Веб-программистов с обучением и трудоустройством')

@section('body')

    <header class="xs-header header-transparent nav-lights">
        <div class="container">
            <nav class="xs-menus clearfix xs_nav-landscape">
                <div class="nav-header">
                    <a class="nav-brand" href="/">
                        <img src="assets/images/logo.png" alt="" draggable="false">
                    </a>
                    <div class="nav-toggle"></div>
                </div>
                <div class="nav-menus-wrapper align-to-right"><span class="nav-menus-wrapper-close-button">✕</span>
                    <!-- menu list -->
                    <ul class="nav-menu">
                        <li class="">
                            <a href="tel:+998935168228"> <i class="fa fa-phone" aria-hidden="true"></i> +998 (93) 516-82-28</a>
                        </li>

                        <li class="">
                            <a href="/#about">О СТУДИИ</a>
                        </li>
                        <li>
                            <a href="/services">УСЛУГИ</a>
                        </li>
                        <li>
                            <a href="/#contacts">КОНТАКТЫ</a>
                        </li>
                    </ul>
                    <!-- End menu list -->
                </div>
                <div class="nav-overlay-panel"></div></nav>
        </div><!-- .container END -->
    </header>


    <div class="xs-inner-banner inner-banner2">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mx-auto">
                    <div class="inner-banner" style="margin-top: 61.5px;">
                        <h2 class="inner-banner-title">
                            Мы набираем команду
                        </h2>

                        <ul class="breadcumbs list-inline">
                            <li><a href="/">Главная</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="xs-section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto" style="color: #000">
                    <div class="from-wraper" style="margin-top: 20px;">
                        <h3 class="text-center" style="margin-bottom: 20px;">
                            Набираем группу начинающих <br> веб-программистов
                        </h3>

                        <p>
                            <strong>Обучим и трудоустроим</strong> - Попробуйте себя в актуальной профессии и найдите первую работу, овладев базовыми навыками в веб-программировании
                        </p>

                        <p>
                            <strong>Чему Вы научитесь</strong>
                            <ul style="list-style:circle inside;">
                                <li> - Основам веб-разработки на PHP;</li>
                                <li> - Создавать простые сайты интернет-магазинов с помощью шаблонов и без них;</li>
                                <li> - Оформлять сайты с помощью CSS;</li>
                                <li> - Разрабатывать сайты на основных CMS и фреймворках;</li>
                                <li> - Размещать сайты в интернете.</li>
                            </ul>
                        </p>

                        <form action="/request-hiring" class="request-hiring contact-form style2" method="post" enctype="multipart/form-data">
                            @csrf

                            <p>
                                <strong class="widget-title">Отправить заявку</strong>
                            </p>

                            @if (session('request-message'))
                                <div class="alert alert-success">
                                    {{ session('request-message') }}
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    <input type="text" placeholder="Имя" name="name" id="xs_contact_name" class="form-control @error('name') invaild @enderror" value="{{ old('name') }}" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <span class="text-danger">{{ $errors->first('phonenumber') }}</span>
                                    <input type="number" placeholder="Номер телефона" name="phonenumber" id="xs_contact_number" class="form-control @error('phonenumber') invaild @enderror" value="{{ old('phonenumber') }}" maxlength="20">
                                </div>
                                <div class="col-lg-6">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    <input type="email" placeholder="Email" name="email" id="xs_contact_email" class="form-control @error('email') invaild @enderror" value="{{ old('email') }}" maxlength="50" required>
                                </div>
                            </div>

                            <div>
                                <span class="text-danger">{{ $errors->first('place') }}</span>
                                <input type="text" placeholder="Город и район где вы проживаете" name="place" id="input_place" class="form-control @error('place') invaild @enderror" value="{{ old('place') }}" maxlength="255">
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div style="margin-bottom: 20px">
                                        <label for="">
                                            <strong>Знание языков</strong>
                                        </label>
                                        <div>
                                            <span class="text-danger">{{ $errors->first('language') }}</span>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="language[russian]" value="1" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Русский
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="language[english]" value="1" id="defaultCheck2">
                                            <label class="form-check-label" for="defaultCheck2">
                                                Английский
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="language[uzbek]" value="1" id="defaultCheck3">
                                            <label class="form-check-label" for="defaultCheck3">
                                                Узбекский
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-7">
                                    <div style="margin-bottom: 20px">
                                        <label for="">
                                            <strong>Чем вы на данный момент занимаетесь ?</strong>
                                        </label>
                                        <div>
                                            <span class="text-danger">{{ $errors->first('what_does') }}</span>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="what_does" value="work" id="defaultCheck4">
                                            <label class="form-check-label" for="defaultCheck4">
                                                Работаю
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="what_does" value="study" id="defaultCheck5">
                                            <label class="form-check-label" for="defaultCheck5">
                                                Учусь (в университете или в лицее/колледже)
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="what_does" value="study_work" id="defaultCheck6">
                                            <label class="form-check-label" for="defaultCheck6">
                                                Работаю и учусь
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-bottom: 20px">
                                <label for="">
                                    <strong>Есть ли опыт работы веб разработчиком ?</strong>
                                </label>
                                <div>
                                    <span class="text-danger">{{ $errors->first('experience') }}</span>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="experience" value="yes" id="defaultCheck7">
                                    <label class="form-check-label" for="defaultCheck7">
                                        Да работал(а)
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="experience" value="no" id="defaultCheck8">
                                    <label class="form-check-label" for="defaultCheck8">
                                        Нет, не работал(а)
                                    </label>
                                </div>
                            </div>

                            <div style="margin-bottom: 20px">
                                <label for="">
                                    <strong>Что из списка вам знакомо и вы это использовали в своих проектах ?</strong>
                                </label>
                                <div>
                                    <span class="text-danger">{{ $errors->first('knowledge') }}</span>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[HTML]" id="HTML">
                                    <label class="form-check-label" for="HTML">
                                        HTML
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[PHP]" id="PHP">
                                    <label class="form-check-label" for="PHP">
                                        PHP
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[JavaScript]" id="JavaScript">
                                    <label class="form-check-label" for="JavaScript">
                                        JavaScript
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[jQuery]" id="jQuery">
                                    <label class="form-check-label" for="jQuery">
                                        jQuery
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[CSS]" id="CSS">
                                    <label class="form-check-label" for="CSS">
                                        CSS
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[Bootstrap]" id="Bootstrap">
                                    <label class="form-check-label" for="Bootstrap">
                                        Bootstrap
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[Wordpress]" id="Wordpress">
                                    <label class="form-check-label" for="Wordpress">
                                        Wordpress
                                    </label>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="knowledge[Drupal]" id="Drupal">
                                    <label class="form-check-label" for="Drupal">
                                        Drupal
                                    </label>
                                </div>
                            </div>


                            <div style="margin-bottom: 20px">
                                <p>
                                    <strong>Тестовое задание</strong>
                                </p>

                                <p>Гостевая книга</p>

                                <p>Гостевая книга предоставляет возможность пользователям сайта оставлять сообщения на сайте. Все данные введенные пользователем сохраняются в БД MySQL, так же в базе данных сохраняются данные о IP пользователя и его браузере.
                                    Форма добавления записи в гостевую книгу должна иметь следующие поля:</p>

                                - User Name (цифры и буквы латинского алфавита) – обязательное поле <br/>
                                - E-mail (формат email) — обязательное поле <br/>
                                - Homepage (формат url) – необязательное поле <br/>
                                - Text (непосредственно сам текст сообщения, HTML тэги недопустимы) – обязательное поле <br/>
                                <br>
                                Сообщения должны выводится в виде таблицы, с возможностью сортировки по следующим полям: User Name, e-mail, и дата добавления (как в порядке убывания, так и в обратном). Сообщения должны разбиваться на страницы по 25 сообщений на каждой.
                                <br>
                                <strong>Выполненное задание т.е. код выполненного задания заархивировать и прикрепить ниже</strong>
                            </div>

                            <div>
                                <label>
                                    <strong>Код выполненного задания:</strong>
                                    <span class="text-danger">{{ $errors->first('filetask') }}</span>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="filetask">
                                        <label class="custom-file-label @error('filetask') invaild @enderror" for="filetask">Выбрать файл</label>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <span class="text-danger">{{ $errors->first('massage') }}</span>
                                <textarea name="massage" id="x_contact_massage" placeholder="Дополнительная информация" class="form-control @error('massage') invaild @enderror" cols="30" rows="10">{{ old('massage') }}</textarea>
                            </div>

                            <div class="captcha">
                                <label>
                                    <strong>Captcha:</strong>
                                    <span class="text-danger">{{ $errors->first('captcha') }}</span>
                                </label>
                                <div class="input-group">
                                    <img style="height: 54px" src="<?=captcha_src();?>" alt="">
                                    <input type="text" name="captcha" class="form-control @error('captcha') invaild @enderror" placeholder="Введите текст с изображения слева" maxlength="5">
                                </div>
                            </div>

                            <div class="btn-wraper">
                                <input type="submit" name="submit" id="xs_contact_submit" class="btn btn-primary" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- .row END -->
        </div>
    </div>

    <!-- footer section start -->
    <footer id="contacts" class="xs-footer-section footer-style5" data-delighter="start:0.80">
        <div class="footer-top-area">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-lg-3">
                        <div class="footer-widget">
                            <h4 class="widget-title">
                                Конакты
                            </h4>
                            <p>
                                Tel.:<a href="tel:+998935168228"> +998 (93) 516-82-28</a> <br>
                                Mail : <a href="mailto:info@alls.uz">info@alls.uz</a>
                            </p>
                            <!--
                            <ul class="social-list">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            -->
                        </div>
                    </div>

                </div><!-- .row END -->
                <div class="row">
                    <div class="col-12 text-center" style="padding-top: 60px;">
                        Copyright <?=date('Y')?> ALLS.
                    </div>
                </div>
            </div><!-- .container END -->
        </div><!-- .footer-top-area END -->

    </footer>
    <!-- footer section end -->
@endsection