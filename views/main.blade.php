<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>
        @yield('title')
    </title>

    <meta name="description" content="@yield('description')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CRoboto:400,500,700,900%7CPlayfair+Display:400,700,700i,900,900i%7CWork+Sans:400,500,600,700" rel="stylesheet">

    <!-- signatra-font -->
    <link rel="stylesheet" href="assets/css/signatra-font.css">

    <link rel="icon" type="image/png" href="favicon.ico">
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="/assets/css/main.css" />
</head>
<body>
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- prelaoder -->
<!-- <div id="preloader">
<div class="preloader-wrapper">
    <div class="spinner"></div>
</div>
<div class="preloader-cancel-btn">
    <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
</div>
</div> -->
<!-- END prelaoder -->

@yield('body')

<!-- js file start -->
<script src="/assets/js/all.js"></script>		<!-- End js file -->

<script>
  function onLoad() {
    if (document.URL.indexOf('#') > -1) {

      jQuery('html, body').animate({
        scrollTop: ($(window.location.hash).offset().top + 1000)
      }, 1000);
    }
  }

  if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);else onLoad();
</script>

</body>
</html>