@extends('main')

@section('title', 'Создание сайтов в Ташкенте. Разработка сайтов - заказать в веб студии ALLS')

@section('description', 'Качественная разработка сайтов в Ташкенте. Студия ALLS предлагает услуги разработки сайтов под ключ. Доступные цены. ☎ +998 (93) 516-82-28')

@section('body')
<!-- header section -->
<header class="xs-header header-transparent nav-lights">
    <div class="container">
        <nav class="xs-menus clearfix">
            <div class="nav-header">
                <a class="nav-brand" href="/">
                    <img src="assets/images/logo.png" alt="">
                </a>
                <div class="nav-toggle"></div>
            </div>
            <div class="nav-menus-wrapper align-to-right">
                <!-- menu list -->
                <ul class="nav-menu">
                    <li>
                        <a href="tel:+998935168228"> <i class="fa fa-phone" aria-hidden="true"></i> +998 (93) 516-82-28</a>
                    </li>

                    <li>
                        <a class="pulsate" style="font-weight: bold; color: red;" href="/hiring">Мы нанимаем</a>
                    </li>

                    <li>
                        <a href="#about">О СТУДИИ</a>
                    </li>
                    <li>
                        <a href="/services">УСЛУГИ</a>
                    </li>
                    <li>
                        <a href="#contacts">КОНТАКТЫ</a>
                    </li>
                </ul>
                <!-- End menu list -->
            </div>
        </nav>
    </div><!-- .container END -->
</header><!-- End header section -->

<!-- agency banner section -->
<div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="agency-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.8 fullwidth mode -->
    <div id="rev_slider_16_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8">
        <ul>	<!-- SLIDE  -->
            <li data-index="rs-47" data-transition="slideup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="assets/images/rev-slider/index1-bg.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                     id="slide-47-layer-2"
                     data-x="['left','left','left','left']" data-hoffset="['509','509','509','509']"
                     data-y="['top','top','top','top']" data-voffset="['116','116','116','116']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="normal"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5;"><img src="assets/images/rev-slider/bg_particle.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-3"
                     data-x="['left','left','left','left']" data-hoffset="['711','711','711','711']"
                     data-y="['top','top','top','top']" data-voffset="['134','134','134','134']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="normal"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":800,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 6;"><img src="assets/images/rev-slider/image_basement.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                     id="slide-47-layer-4"
                     data-x="['left','left','left','left']" data-hoffset="['684','684','684','684']"
                     data-y="['top','top','top','top']" data-voffset="['551','551','551','551']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="normal"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":1100,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7;"><img src="assets/images/rev-slider/cloud_v1.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                     id="slide-47-layer-5"
                     data-x="['left','left','left','left']" data-hoffset="['1115','1115','1115','1115']"
                     data-y="['top','top','top','top']" data-voffset="['663','663','663','663']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="normal"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":1400,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 8;"><img src="assets/images/rev-slider/cloud_v2.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-9"
                     data-x="['left','left','left','left']" data-hoffset="['876','876','876','876']"
                     data-y="['top','top','top','top']" data-voffset="['157','157','157','157']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":1700,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 9;">
                    <div class="rs-looped rs-wave"  data-speed="2" data-angle="0" data-radius="5px" data-origin="50% 50%"><img src="assets/images/rev-slider/girl_v2.png" alt="" data-ww="" data-hh="" data-no-retina> </div></div>

                <!-- LAYER NR. 6 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-8"
                     data-x="['left','left','left','left']" data-hoffset="['1034','1034','1034','1034']"
                     data-y="['top','top','top','top']" data-voffset="['224','224','224','224']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 10;"><img src="assets/images/rev-slider/girl_v3.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-6"
                     data-x="['left','left','left','left']" data-hoffset="['747','747','747','747']"
                     data-y="['top','top','top','top']" data-voffset="['401','401','401','401']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":2300,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 11;"><img src="assets/images/rev-slider/man_v1.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 8 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-7"
                     data-x="['left','left','left','left']" data-hoffset="['797','797','797','797']"
                     data-y="['top','top','top','top']" data-voffset="['467','467','467','467']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-visibility="['on','on','off','off']"
                     data-type="image"
                     data-responsive_offset="on"

                     data-frames='[{"delay":2600,"speed":1000,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 12;"><img src="assets/images/rev-slider/girl_v1.png" alt="" data-ww="" data-hh="" data-no-retina> </div>

                <!-- LAYER NR. 9 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-47-layer-1"
                     data-x="['left','left','left','left']" data-hoffset="['65','65','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="579"
                     data-height="320"
                     data-whitespace="normal"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":2900,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 13; min-width: 579px; max-width: 579px; white-space: normal; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><div class="agency-banner-content">
                        <h1 class="banner-title">Создание сайтов под ключ</h1>
                        <a href="#xs-contact-form" class="btn btn-primary">ЗАКАЗАТЬ</a>
                    </div><!-- .agency-banner-content END -->
                </div>
            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
</div><!-- END REVOLUTION SLIDER --><!-- end agency banner section -->

<!-- agency feature section -->
<section id="about" class="xs-section-padding xs-pb-0 waypoint-tigger">
    <div class="container">
        <div class="about-header col-xs-12">
            <h1 class="text-center">О студии</h1>
        </div>

        <div class="agency-feature-group">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="agency-feature-box text-center">
                        <div class="xs-svg">
                            <img src="assets/images/feature-icon/agency-feature-icon-1.png" alt="">
                        </div>
                        <h2 class="xs-title">
                            <a href="#">Дизайн</a>
                        </h2>
                        <p>
                            Мы разрабатываем интернет-магазины, визитки и корпоративные сайты с учетом трендов проектирования интерфейсов и эргономики.
                        </p>
                    </div><!-- .agency-feature-box END end -->
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="agency-feature-box text-center">
                        <div class="xs-svg">
                            <img src="assets/images/feature-icon/agency-feature-icon-2.png" alt="">
                        </div>
                        <h2 class="xs-title">
                            <a href="#">Разработка веб сайтов</a>
                        </h2>
                        <p>Полностью кастомная разработка нестандартных решений. Сделаем все, что угодно, если это вообще в силах человеческих.</p>
                    </div><!-- .agency-feature-box END end -->
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="agency-feature-box text-center">
                        <div class="xs-svg">
                            <img src="assets/images/feature-icon/agency-feature-icon-3.png" alt="">
                        </div>
                        <h2 class="xs-title">
                            <a href="#">Интернет-маркетинг в Ташкенте</a>
                        </h2>
                        <p>Внедрим для вашего бизнеса комплексный входящий интернет маркетинг.</p>
                    </div><!-- .agency-feature-box END end -->
                </div>
            </div><!-- .row END -->
        </div>
    </div><!-- .container END -->
</section><!-- end agency feature section -->

<!-- agency creative comunication section -->
<section class="agency-creative-comunication xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="creative-comunication-image">
                    <img src="assets/images/creative-comunications.png" alt="">
                </div><!-- .creative-comunication-image END -->
            </div>
            <div class="col-md-6">
                <div class="creative-comunication-content">
                    <div class="agency-section-title">
                        <h2 class="section-title">НАША <em>СПЕЦИАЛИЗАЦИЯ</em></h2>
                    </div>
                    <p>Бизнес-аналитика, написание ТЗ, прототипирование, веб-дизайн, верстка, программирование, тестирование и поддержка проектов, включая дальнейшее продвижение и интернет-маркетинг. </p>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="media round-media">
                                <i class="icon icon-creative_communication_1"></i>
                                <div class="media-body">
                                    <p>Правильный расчет времени </p>
                                </div>
                            </div><!-- .round-media END -->
                            <div class="media round-media color-1">
                                <i class="icon icon-creative_communication_2"></i>
                                <div class="media-body">
                                    <p>Уведомления об обновлениях</p>
                                </div>
                            </div><!-- .round-media .color-1 END -->
                        </div>
                        <div class="col-lg-6">
                            <div class="media round-media color-2">
                                <i class="icon icon-creative_communication_3"></i>
                                <div class="media-body">
                                    <p>Всегда на связи</p>
                                </div>
                            </div><!-- .round-media .color-2 END -->
                            <div class="media round-media color-3">
                                <i class="icon icon-creative_communication_4"></i>
                                <div class="media-body">
                                    <p>Фото и видео</p>
                                </div>
                            </div><!-- .round-media .color-3 END -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- end agency creative comunication section -->

<!-- agency parallax area section -->
<section class="agency-parallax-area shuffle-title">
    <div class="shuffle-letter-title-wraper">
        <h2 class="shuufle-letter-title">МИССИЯ</h2>
    </div>
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-7">
                <div class="parallax-slide" style="background-image: url(assets/images/paralax-bg/paralax-bg-1.png)"></div>
            </div>
            <div class="col-lg-5">
                <div class="parallax-container-content">
                    <img src="assets/images/thumbs-up-icon.png" alt="">
                    <div class="agency-section-title">
                        <h2 class="section-title"> Наша <em>миссия</em></h2>
                    </div>
                    <p>Способствовать развитию и продвижению узбекских компаний в Интернете, улучшая качество предоставляемых услуг и повышая их конкурентоспособность и устойчивость.</p>
                    <a href="contact.html" class="btn btn-primary">ЗАКАЗАТЬ ЗВОНОК</a>
                </div><!-- .parallax-container-content END -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section>

<!-- footer section start -->
<footer id="contacts" class="xs-footer-section footer-style5" data-delighter="start:0.80">
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center">Связаться снами</h1>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 col-lg-3">
                    <div class="footer-widget">
                        <h4 class="widget-title">
                            Конакты
                        </h4>
                        <p>
                            Tel.:<a href="tel:+998935168228"> +998 (93) 516-82-28</a> <br>
                            Mail : <a href="mailto:info@alls.uz">info@alls.uz</a>
                        </p>
                        <!--
                        <ul class="social-list">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                        -->
                    </div>
                </div>

                <div class="col-lg-8 mx-auto">

                    <div class="form-wrapper footer-widget">
                        <form action="/request" id="xs-contact-form" class="contact-form style2" method="post" enctype="multipart/form-data">
                            @csrf

                            <h4 class="widget-title">Заказать проект</h4>

                            @if (session('request-message'))
                                <div class="alert alert-success">
                                    {{ session('request-message') }}
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-12">
                                    <a href="/brief.docx" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Скачать бриф</a>
                                    <p class="small" style="margin-bottom: 20px;">
                                        Бриф – это опросная анкета или техническое задание, которое максимально передает нам исходные данные.
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    <input type="text" placeholder="Имя" name="name" id="xs_contact_name" class="form-control @error('name') invaild @enderror" value="{{ old('name') }}" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <span class="text-danger">{{ $errors->first('phonenumber') }}</span>
                                    <input type="number" placeholder="Номер телефона" name="phonenumber" id="xs_contact_number" class="form-control @error('phonenumber') invaild @enderror" value="{{ old('phonenumber') }}" maxlength="20">
                                </div>
                                <div class="col-lg-6">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    <input type="email" placeholder="Email" name="email" id="xs_contact_email" class="form-control @error('email') invaild @enderror" value="{{ old('email') }}" maxlength="50" required>
                                </div>
                            </div>

                            <div>
                                <span class="text-danger">{{ $errors->first('subject') }}</span>
                                <input type="text" placeholder="Тема" name="subject" id="xs_contact_subject" class="form-control @error('subject') invaild @enderror" value="{{ old('subject') }}" maxlength="255" required>
                            </div>

                            <div>
                                <span class="text-danger">{{ $errors->first('massage') }}</span>
                                <textarea name="massage" id="x_contact_massage" placeholder="Описание" class="form-control @error('massage') invaild @enderror" cols="30" rows="10">{{ old('massage') }}</textarea>
                            </div>

                            <div>
                                <label>
                                     <strong>Бриф:</strong>
                                    <span class="text-danger">{{ $errors->first('brief') }}</span>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="brief">
                                        <label class="custom-file-label @error('brief') invaild @enderror" for="brief">Выбрать файл</label>
                                    </div>
                                </div>
                            </div>

                            <div class="captcha">
                                <label>
                                    <strong>Captcha:</strong>
                                    <span class="text-danger">{{ $errors->first('captcha') }}</span>
                                </label>
                                <div class="input-group">
                                    <img style="height: 54px" src="<?=captcha_src();?>" alt="">
                                    <input type="text" name="captcha" class="form-control @error('captcha') invaild @enderror" placeholder="Введите текст с изображения слева" maxlength="5">
                                </div>
                            </div>

                            <div class="btn-wraper">
                                <input type="submit" name="submit" id="xs_contact_submit" class="btn btn-primary" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- .row END -->
            <div class="row">
                <div class="col-12 text-center" style="padding-top: 60px;">
                    Copyright <?=date('Y')?> ALLS.
                </div>
            </div>
        </div><!-- .container END -->
    </div><!-- .footer-top-area END -->

</footer>
<!-- footer section end -->

@endsection