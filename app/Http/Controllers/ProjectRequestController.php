<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProjectRequestController extends Controller
{
  /**
   * Store a new user.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(ProjectRequest $request)
  {
    $file = $request->file('brief');
    $path = '';
    if (!empty($file)) {
      $path = $file->store('requests');
    }

    $insertArray = [
      'name' => $request->input('name'),
      'phonenumber' => $request->input('phonenumber'),
      'email' => $request->input('email'),
      'subject' => $request->input('subject'),
      'massage' => $request->input('massage'),
      'created_at' => date('Y-m-d H:i:s'),
      'file' => $path,
    ];
    DB::table('project_request')->insert($insertArray);

    $to_name = '';
    $to_email = 'sodiq915@gmail.com';
    $data = array(
      'name' => "Alls.uz",
      "body" => $insertArray
    );
    Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
      $message->to($to_email, $to_name)
        ->subject('Уведомление allsuz');
      $message->from('allsuz.info@gmail.com', 'Alls.uz');
    });

    return back()->with('request-message', 'Спасибо заявка принята');
  }

}