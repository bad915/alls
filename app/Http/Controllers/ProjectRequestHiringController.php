<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequestHiring;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProjectRequestHiringController extends Controller
{
  /**
   * Store a new user.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(ProjectRequestHiring $request)
  {
    $path = $request->file('filetask')->store('requests-hiring');
    $arrayRequest = $request->toArray();

    $insertArray = [
      'name' => $request->input('name'),
      'place' => $request->input('place'),
      'phonenumber' => $request->input('phonenumber'),
      'email' => $request->input('email'),
      'massage' => $request->input('massage'),
      'created_at' => date('Y-m-d H:i:s'),
      'filetask' => $path,
      'language' => !empty($arrayRequest['language']) ? implode('|', array_keys($arrayRequest['language'])) : NULL,
      'what_does' => $arrayRequest['what_does'],
      'experience' => $arrayRequest['experience'],
      'knowledge' => !empty($arrayRequest['knowledge']) ? implode('|', array_keys($arrayRequest['knowledge'])) : NULL,
    ];

    DB::table('request_hiring')->insert($insertArray);

    $to_name = '';
    $to_email = 'sodiq915@gmail.com,kildee@yandex.ru';
    $data = array(
      'name' => "Alls.uz",
      "body" => $insertArray
    );
    Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
      $message->to($to_email, $to_name)
        ->cc('kildee@yandex.ru')
        ->subject('Уведомление allsuz');
      $message->from('allsuz.info@gmail.com', 'Alls.uz');
    });

    return back()->with('request-message', 'Спасибо заявка принята');
  }

}