<?php
namespace App\Http\Middleware;

use Closure;

class BeforeAutoTrimmer {

  public function handle($request, Closure $next)
  {
    $params = $request->all();

    foreach ($params as $key => $val) {
      if (is_scalar($val)) {
        $params[$key] = trim($val);
      }
    }

    $request->merge($params);
    return $next($request);
  }
}