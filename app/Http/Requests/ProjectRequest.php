<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class ProjectRequest extends FormRequest
{

  use SanitizesInput;

  protected $redirect = '/#contacts';

  /**
   * For more sanitizer rule check https://github.com/Waavi/Sanitizer
   */
  public function validateResolved()
  {
    {
      $this->sanitize();
      parent::validateResolved();
    }
  }

  public function attributes()
  {
    return [
      'name' => 'Имя',
      'captcha' => 'Капча',
      'phonenumber' => 'Номер телефона',
      'email' => 'Адрес элетронной почты',
      'subject' => 'Тема',
    ];
  }

  public function rules()
  {
    return [
      'name' => 'required|max:255',
      'captcha' => 'required|captcha|max:5',
      'email' => 'required|email|max:50',
      'subject' => 'required|max:255',
      'phonenumber' => 'nullable|max:255',
      'brief' => 'file|mimes:doc,docx,pdf,txt|max:5120',
    ];
  }

  public function messages()
  {
    return [
      'name.required' => 'Поле имя обязательно для заполнения',
      'captcha.required'  => 'Пожалуйтса заполните капчу правильно',
      'captcha.captcha'  => 'Пожалуйтса заполните капчу правильно',
      'email.required'  => 'Поле email обязательно для заполнения',
      'email.email'  => 'Пожалуйтса введите корретный почтовый адрес',
      'subject.required'  => 'Пожалуйтса тема обязательно для заполнения',
      'brief.mimes'  => 'Принимается только форматы doc,docx,pdf,txt',
    ];
  }
}