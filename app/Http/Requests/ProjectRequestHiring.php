<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class ProjectRequestHiring extends FormRequest
{

  use SanitizesInput;

  protected $redirect = '/hiring';

  /**
   * For more sanitizer rule check https://github.com/Waavi/Sanitizer
   */
  public function validateResolved()
  {
    {
      $this->sanitize();
      parent::validateResolved();
    }
  }

  public function attributes()
  {
    return [
      'name' => 'Имя',
      'captcha' => 'Капча',
      'phonenumber' => 'Номер телефона',
      'email' => 'Адрес элетронной почты',
    ];
  }

  public function rules()
  {
    return [
      'name' => 'required|max:255',
      'captcha' => 'required|captcha|max:5',
      'email' => 'required|email|max:50',
      'phonenumber' => 'nullable|max:255',
      'filetask' => 'required|file|mimes:zip,rar,gz|max:5120',
      'language' => 'required',
      'what_does' => 'required',
      'experience' => 'required',
    ];
  }

  public function messages()
  {
    return [
      'name.required' => 'Поле имя обязательно для заполнения',
      'captcha.required'  => 'Пожалуйтса заполните капчу правильно',
      'captcha.captcha'  => 'Пожалуйтса заполните капчу правильно',
      'email.required'  => 'Поле email обязательно для заполнения',
      'email.email'  => 'Пожалуйтса введите корретный почтовый адрес',
      'filetask.mimes'  => 'Принимается только форматы .zip, .rar, .gz',
      'filetask.required'  => 'Задание обязательное поле',
      'language.required'  => 'Обязательное поле',
      'what_does.required'  => 'Обязательное поле',
      'experience.required'  => 'Обязательное поле',
    ];
  }
}